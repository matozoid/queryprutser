package com.laamella.queryprutser.hql;

public class InsertIntoClause<T> extends ClauseWithRootEntity<T> {
    InsertIntoClause(final Class<T> rootClass, final CharSequence alias) {
        super("insert into ", "", "", rootClass, alias);
    }
}
