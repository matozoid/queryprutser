package com.laamella.queryprutser.hql;

import com.laamella.queryprutser.common.Assert;
import com.laamella.queryprutser.common.SeparatedStringBuilder;

public class ClauseWithRootEntity<T> extends SeparatedStringBuilder {
    ClauseWithRootEntity(final CharSequence prefix, final CharSequence infix, final CharSequence postfix, final Class<T> rootEntity, final CharSequence alias) {
        super(prefix, infix, postfix);
        add(createEntityString(rootEntity, alias));
    }

    private CharSequence createEntityString(Class<T> rootClass, CharSequence alias) {
        Assert.notNull(rootClass);
        StringBuilder stringBuilder = new StringBuilder(rootClass.getCanonicalName());
        if (alias != null) {
            stringBuilder.append(" ");
            stringBuilder.append(alias);
        }
        stringBuilder.append(" ");
        return stringBuilder;
    }
}
