package com.laamella.queryprutser.hql;

/**
 * Main user entry point for the HQL functionality.
 */
public final class HqlPrutser {
    public static <T> Select<T> select(final Class<T> rootClass, final CharSequence alias) {
        return new Select<T>(rootClass, alias);
    }

    public static <T> Select<T> select(final Class<T> rootClass) {
        return select(rootClass, null);
    }

    public static <T, U> Insert<T, U> insert(final Class<T> intoRootEntity, final CharSequence intoAlias, final Class<U> selectRootEntity, final CharSequence selectAlias) {
        return new Insert<T, U>(intoRootEntity, intoAlias, selectRootEntity, selectAlias);
    }

    public static <T, U> Insert<T, U> insert(final Class<T> intoRootEntity, final Class<U> selectRootEntity) {
        return insert(intoRootEntity, null, selectRootEntity, null);
    }

    public static Delete delete() {
        return new Delete();
    }

    public static Update update() {
        return new Update();
    }
}
