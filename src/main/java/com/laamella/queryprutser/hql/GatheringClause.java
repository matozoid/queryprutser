package com.laamella.queryprutser.hql;

public class GatheringClause extends ClauseWithSubselects {
	protected GatheringClause(final CharSequence keyword) {
		super(keyword + " ", ", ", " ");
	}

	public void min(final CharSequence string) {
		add("min(" + string + ")");
	}

	public void max(final CharSequence string) {
		add("max(" + string + ")");
	}

	public void avg(final CharSequence string) {
		add("avg(" + string + ")");
	}

	public void sum(final CharSequence string) {
		add("sum(" + string + ")");
	}

	public void count(final CharSequence string) {
		add("count(" + string + ")");
	}
}
