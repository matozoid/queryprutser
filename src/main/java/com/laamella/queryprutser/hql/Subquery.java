package com.laamella.queryprutser.hql;

import com.laamella.queryprutser.common.Assert;

public class Subquery<T> extends Select<T> {
	private CharSequence before = "";
	private CharSequence after = "";

	Subquery(final Class<T> rootClass, final CharSequence alias) {
		super(rootClass, alias);
	}

	public Subquery<T> before(final CharSequence before) {
		Assert.notNull(before);
		this.before = before;
		return this;
	}

	public Subquery<T> after(final CharSequence after) {
		Assert.notNull(after);
		this.after = after;
		return this;
	}

	@Override
	public CharSequence queryText() {
		final StringBuilder hql = new StringBuilder();
		hql.append(before);
		hql.append("(");
		hql.append(super.queryText());
		hql.append(") ");
		hql.append(after);
		return hql;
	}
}
