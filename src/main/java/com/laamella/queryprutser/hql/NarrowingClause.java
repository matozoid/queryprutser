package com.laamella.queryprutser.hql;

public class NarrowingClause extends ClauseWithSubselects {
	protected NarrowingClause(final CharSequence keyword) {
		super(" \n" + keyword + " ", "\n", " ");
	}

	public void and(final CharSequence... values) {
		addWithSpecialInfix("\nand ", values);
	}

	public void or(final CharSequence... values) {
		addWithSpecialInfix("\nor ", values);
	}
}
