package com.laamella.queryprutser.hql;

import java.util.Map;

import org.hibernate.Query;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.laamella.queryprutser.common.Statement;

public abstract class HqlStatement extends Statement {
    private final Logger log = LoggerFactory.getLogger(HqlStatement.class);

    /**
     * @return create the final query.
     */
    public final Query query(final Session session) {
        final String hql = queryText().toString();
        log.debug(hql);
        final Query query = session.createQuery(hql);
        query.setProperties(allParameters());

        if (log.isDebugEnabled()) {
            for (final Map.Entry<String, Object> entry : allParameters().entrySet()) {
                log.debug(entry.getKey() + "=" + entry.getValue());
            }
        }

        return query;
    }


}
