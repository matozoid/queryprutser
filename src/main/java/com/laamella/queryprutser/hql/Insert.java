package com.laamella.queryprutser.hql;

import java.util.Map;

import com.laamella.queryprutser.common.SeparatedStringBuilder;

/**
 * Simple helper for building dynamic HQL insert queries, as opposed to
 * Criteria, which are more helpful, complex, and contain many gotcha's, and to
 * plain HQL, which is not dynamic.
 */
public class Insert<T, U> extends HqlStatement {
    private final InsertIntoClause<T> insertEntityPart;
    public final SeparatedStringBuilder insertInto = new SeparatedStringBuilder("(", ", ", ") ");
    public final Select<U> select;

     Insert(final Class<T> intoRootEntity, final CharSequence intoAlias, final Class<U> selectRootEntity, final CharSequence selectAlias) {
        insertEntityPart = new InsertIntoClause<T>(intoRootEntity, intoAlias);
        select = new Select<U>(selectRootEntity, selectAlias);
    }

    /**
     * @return all parameters that occur in the query and its subqueries.
     */
    @Override
	public Map<String, Object> allParameters() {
        final Map<String, Object> allParameters = super.allParameters();
        select.putAllParametersInThisMap(allParameters);
        return allParameters;
    }

    /**
     * @return the current HQL for the query.
     */
    @Override
	public CharSequence queryText() {
        final StringBuilder hql = new StringBuilder();
        hql.append(insertEntityPart);
        hql.append(insertInto);
        hql.append(select);
        return hql;
    }
}
