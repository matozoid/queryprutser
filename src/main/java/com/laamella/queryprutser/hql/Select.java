package com.laamella.queryprutser.hql;

import java.util.Map;

/**
 * Simple helper for building dynamic HQL select queries, as opposed to
 * Criteria, which are more helpful, complex, and contain many gotcha's, and to
 * plain HQL, which is not dynamic.
 */
public class Select<T> extends HqlStatement {
	public final GatheringClause select = new GatheringClause("select");
	public final FromClause<T> from;
	public final NarrowingClause where = new NarrowingClause("where");
	private final GatheringClause groupBy = new GatheringClause("\ngroup by");
	public final NarrowingClause having = new NarrowingClause("having");
	private final GatheringClause orderBy = new GatheringClause("order by ");

	Select(final Class<T> rootClass, final CharSequence alias) {
		from = new FromClause<T>(rootClass, alias);
	}

	public Select<T> orderBy(final CharSequence... fields) {
		orderBy.add(fields);
		return this;
	}

	public Select<T> groupBy(final CharSequence... fields) {
		groupBy.add(fields);
		return this;
	}

	public Select<T> select(final CharSequence... fields) {
		select.add(fields);
		return this;
	}

	/**
	 * @return all parameters that occur in the query and its subqueries.
	 */
	@Override
	public Map<String, Object> allParameters() {
		final Map<String, Object> allParameters = super.allParameters();
		where.putAllParametersInThisMap(allParameters);
		select.putAllParametersInThisMap(allParameters);
		having.putAllParametersInThisMap(allParameters);
		return allParameters;
	}

	/**
	 * @return the current HQL for the query.
	 */
	@Override
	public CharSequence queryText() {
		final StringBuilder hql = new StringBuilder();
		hql.append(select);
		hql.append(from);
		hql.append(where);
		hql.append(groupBy);
		hql.append(having);
		hql.append(orderBy);
		return hql;
	}

}
