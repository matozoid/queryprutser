package com.laamella.queryprutser.hql;

import com.laamella.queryprutser.common.SeparatedStringBuilder;

import java.util.*;

public class ClauseWithSubselects extends SeparatedStringBuilder {
	private final List<Subquery<?>> subqueries = new ArrayList<Subquery<?>>();

	ClauseWithSubselects(final CharSequence prefix, final CharSequence infix, final CharSequence postfix) {
		super(prefix, infix, postfix);
	}

	public <T> Subquery<T> subselect(final Class<T> rootClass, final CharSequence alias) {
		final Subquery<T> subquery = new Subquery<T>(rootClass, alias);
		subqueries.add(subquery);
		return subquery;
	}

	public <T> Subquery<T> subselect(final Class<T> rootClass) {
		return subselect(rootClass, null);
	}

	@Override
	public String toString() {
		final SeparatedStringBuilder locallCopy = new SeparatedStringBuilder(this);
		for (final Subquery<?> builder : subqueries) {
			locallCopy.add(" " + builder.queryText() + " ");
		}
		return locallCopy.toString();
	}

	void putAllParametersInThisMap(final Map<String, Object> inHere) {
		for (final Subquery<?> subquery : subqueries) {
			inHere.putAll(subquery.allParameters());
		}
	}
}
