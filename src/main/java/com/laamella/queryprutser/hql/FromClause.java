package com.laamella.queryprutser.hql;

public class FromClause<T> extends ClauseWithRootEntity<T> {
    FromClause(final Class<T> rootClass, final CharSequence alias) {
        super("from ", " ", " ", rootClass, alias);
    }

    public FromClause<T> joinFetch(final CharSequence relationship, final CharSequence alias) {
        add("join fetch " + relationship + " " + alias);
        return this;
    }
}
