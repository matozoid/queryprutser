package com.laamella.queryprutser.sql;

import com.laamella.queryprutser.common.Statement;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public abstract class SqlStatement extends Statement {
    public PreparedStatement query(Connection connection) throws SQLException {
        final String query = toString();

        /* Algorithm taken from http://www.javaworld.com/javaworld/jw-04-2007/jw-04-jdbc.html */
        final Map<String, List<Integer>> paramMap = new HashMap<String, List<Integer>>();

        final int length = query.length();
        final StringBuilder parsedQuery = new StringBuilder(length);
        boolean inSingleQuote = false;
        boolean inDoubleQuote = false;
        int index = 1;

        for (int i = 0; i < length; i++) {
            char c = query.charAt(i);
            if (inSingleQuote) {
                if (c == '\'') {
                    inSingleQuote = false;
                }
            } else if (inDoubleQuote) {
                if (c == '"') {
                    inDoubleQuote = false;
                }
            } else {
                if (c == '\'') {
                    inSingleQuote = true;
                } else if (c == '"') {
                    inDoubleQuote = true;
                } else if (c == ':' && i + 1 < length && Character.isJavaIdentifierStart(query.charAt(i + 1))) {
                    int j = i + 2;
                    while (j < length && Character.isJavaIdentifierPart(query.charAt(j))) {
                        j++;
                    }
                    final String name = query.substring(i + 1, j);
                    // replace the parameter with a question mark
                    c = '?';
                    // skip past the end if the parameter
                    i += name.length();

                    List<Integer> indexList = paramMap.get(name);
                    if (indexList == null) {
                        indexList = new LinkedList<Integer>();
                        paramMap.put(name, indexList);
                    }
                    indexList.add(index);

                    index++;
                }
            }
            parsedQuery.append(c);
        }

        final PreparedStatement statement = connection.prepareStatement(parsedQuery.toString());

        for (Map.Entry<String, Object> parameter : allParameters().entrySet()) {
            List<Integer> indexes = paramMap.get(parameter.getKey());
            if (indexes == null) {
                throw new IllegalArgumentException("Parameter not found: " + parameter.getKey());
            }
            for (Integer i : indexes) {
                statement.setObject(i, parameter.getValue());
            }
        }
        return statement;
    }

}
