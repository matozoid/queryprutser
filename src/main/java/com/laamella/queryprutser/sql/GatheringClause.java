package com.laamella.queryprutser.sql;

/**
 * SELECT, ORDER BY, GROUP BY
 */
public class GatheringClause extends ClauseWithSubselects {
	GatheringClause(final String keyword) {
		super(keyword + " ", ", ", " ");
	}

	public GatheringClause min(final String expression) {
		add("MIN(" + expression + ")");
		return this;
	}

	public GatheringClause max(final String expression) {
		add("MAX(" + expression + ")");
		return this;
	}

	public GatheringClause avg(final String expression) {
		add("AVG(" + expression + ")");
		return this;
	}

	public GatheringClause sum(final String expression) {
		add("SUM(" + expression + ")");
		return this;
	}

	public GatheringClause count(final String expression) {
		add("COUNT(" + expression + ")");
		return this;
	}

	public GatheringClause min(final String expression, final String alias) {
		add("MIN(" + expression + ") AS " + alias);
		return this;
	}

	public GatheringClause max(final String expression, final String alias) {
		add("MAX(" + expression + ") AS " + alias);
		return this;
	}

	public GatheringClause avg(final String expression, final String alias) {
		add("AVG(" + expression + ") AS " + alias);
		return this;
	}

	public GatheringClause sum(final String expression, final String alias) {
		add("SUM(" + expression + ") AS " + alias);
		return this;
	}

	public GatheringClause count(final String expression, final String alias) {
		add("COUNT(" + expression + ") AS " + alias);
		return this;
	}
}
