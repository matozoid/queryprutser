package com.laamella.queryprutser.sql;

import com.laamella.queryprutser.common.SeparatedStringBuilder;

public class FromClause extends ClauseWithSubselects {
	FromClause() {
		super("FROM ", " ", " ");
	}

	public FromClause crossJoin(final String table) {
		add("CROSS JOIN " + table);
		return this;
	}

	public FromClause innerJoin(final String table) {
		add("INNER JOIN " + table);
		return this;
	}

	public FromClause unionJoin(final String table) {
		add("UNION JOIN " + table);
		return this;
	}

	public FromClause join(final String table) {
		add("JOIN " + table);
		return this;
	}

	public FromClause outerJoin(final String table) {
		add("OUTER JOIN " + table);
		return this;
	}

	public FromClause leftOuterJoin(final String table) {
		add("LEFT OUTER JOIN " + table);
		return this;
	}

	public FromClause rightOuterJoin(final String table) {
		add("RIGHT OUTER JOIN " + table);
		return this;
	}

	public FromClause fullOuterJoin(final String table) {
		add("FULL OUTER JOIN " + table);
		return this;
	}

	public FromClause on(final String joinOn) {
		add("ON " + joinOn);
		return this;
	}

	public FromClause using(final String... columns) {
		add(new SeparatedStringBuilder("USING (", ", ", ")").add(columns).toCharSequence());
		return this;
	}
}
