package com.laamella.queryprutser.sql;

import java.util.Map;

import com.laamella.queryprutser.common.SeparatedStringBuilder;

/**
 * Build and SQL insert statement. Access through SqlPrutser.
 */
// FIXME
public class InsertBuilder extends SqlStatement {
	private final InsertIntoClause insertTablePart = new InsertIntoClause();
	public final SeparatedStringBuilder into = new SeparatedStringBuilder("(", ", ", ") ");
	public final QueryBuilder select = new QueryBuilder();

	InsertBuilder(String intoTable) {
		insertTablePart.add(intoTable);
	}

	/**
	 * @return all parameters that occur in the query and its subqueries.
	 */
	@Override
	public Map<String, Object> allParameters() {
		final Map<String, Object> allParameters = super.allParameters();
		select.putAllParametersInThisMap(allParameters);
		return allParameters;
	}

	/**
	 * @return the current SQL for the query.
	 */
	@Override
	public CharSequence queryText() {
		final StringBuilder sql = new StringBuilder();
		sql.append(insertTablePart);
		sql.append(into);
		sql.append(select);
		return sql;
	}
}
