package com.laamella.queryprutser.sql;

import com.laamella.queryprutser.common.Assert;

public class Subquery extends QueryBuilder {
	private CharSequence before = "";
	private CharSequence after = "";

	public Subquery(final String... columns) {
		super(columns);
	}

	public Subquery before(final CharSequence before) {
		Assert.notNull(before);
		this.before = before;
		return this;
	}

	public Subquery after(final CharSequence after) {
		Assert.notNull(after);
		this.after = after;
		return this;
	}

	@Override
	public CharSequence queryText() {
		final StringBuilder sql = new StringBuilder();
		sql.append(before);
		sql.append("(");
		sql.append(super.queryText());
		sql.append(") ");
		sql.append(after);
		return sql;
	}
}
