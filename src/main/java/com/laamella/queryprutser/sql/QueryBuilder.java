package com.laamella.queryprutser.sql;

import java.util.Map;

/**
 * Builds an SQL select statement. Access through SqlPrutser.
 */
public class QueryBuilder extends SqlStatement {
	public final GatheringClause select = new GatheringClause("SELECT");
	public final FromClause from = new FromClause();
	public final NarrowingClause where = new NarrowingClause("WHERE");
	public final GatheringClause groupBy = new GatheringClause("\nGROUP BY ");
	public final NarrowingClause having = new NarrowingClause("HAVING");
	public final GatheringClause orderBy = new GatheringClause("\nORDER BY");

	public QueryBuilder(final String... columns) {
		select.add(columns);
	}

	public GatheringClause select(final CharSequence... fields) {
		select.add(fields);
		return select;
	}

	public FromClause from(final CharSequence... fields) {
		from.add(fields);
		return from;
	}

	public NarrowingClause where(final CharSequence... fields) {
		where.add(fields);
		return where;
	}

	public GatheringClause groupBy(final CharSequence... fields) {
		groupBy.add(fields);
		return groupBy;
	}

	public NarrowingClause having(final CharSequence... fields) {
		having.add(fields);
		return having;
	}

	public GatheringClause orderBy(final CharSequence... fields) {
		orderBy.add(fields);
		return orderBy;
	}

	/**
	 * @return all parameters that occur in the query and its subqueries.
	 */
	@Override
	public Map<String, Object> allParameters() {
		final Map<String, Object> allParameters = super.allParameters();
		where.putAllParametersInThisMap(allParameters);
		select.putAllParametersInThisMap(allParameters);
		having.putAllParametersInThisMap(allParameters);
		return allParameters;
	}

	/**
	 * @return the current SQL for the query.
	 */
	@Override
	public CharSequence queryText() {
		final StringBuilder sql = new StringBuilder();
		sql.append(select);
		sql.append(from);
		sql.append(where);
		sql.append(groupBy);
		sql.append(having);
		sql.append(orderBy);
		return sql;
	}

}
