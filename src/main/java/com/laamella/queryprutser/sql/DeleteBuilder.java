package com.laamella.queryprutser.sql;

import java.util.Map;

import com.laamella.queryprutser.common.SeparatedStringBuilder;

public class DeleteBuilder extends SqlStatement {
	public SeparatedStringBuilder from = new SeparatedStringBuilder("DELETE FROM ", ", ", "");
	public NarrowingClause where = new NarrowingClause("WHERE");

	public DeleteBuilder(final String firstTable) {
		from.add(firstTable);
	}

	/**
	 * @return all parameters that occur in the query and its subqueries.
	 */
	@Override
	public Map<String, Object> allParameters() {
		final Map<String, Object> allParameters = super.allParameters();
		where.putAllParametersInThisMap(allParameters);
		return allParameters;
	}

	/**
	 * @return the current HQL for the query.
	 */
	@Override
	public CharSequence queryText() {
		final StringBuilder sql = new StringBuilder();
		sql.append(from);
		sql.append(where);
		return sql;
	}

}
