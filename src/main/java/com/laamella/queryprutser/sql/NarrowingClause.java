package com.laamella.queryprutser.sql;

/**
 * WHERE, HAVING
 */
public class NarrowingClause extends ClauseWithSubselects {
	NarrowingClause(final CharSequence keyword) {
		super(" \n" + keyword + " ", "\n", " ");
	}

	public void and(final CharSequence... values) {
		addWithSpecialInfix("\nAND ", values);
	}

	public void or(final CharSequence... values) {
		addWithSpecialInfix("\nOR ", values);
	}

	public Subquery exists(final String firstColumn) {
		final Subquery subquery = subSelect(firstColumn);
		subquery.before("EXISTS");
		return subquery;
	}
}
