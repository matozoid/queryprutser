package com.laamella.queryprutser.sql;

import java.util.*;

import com.laamella.queryprutser.common.SeparatedStringBuilder;

public class ClauseWithSubselects extends SeparatedStringBuilder {
	private final List<Subquery> subqueries = new ArrayList<Subquery>(0);

	ClauseWithSubselects(final CharSequence prefix, final CharSequence infix, final CharSequence postfix) {
		super(prefix, infix, postfix);
	}

	public Subquery subSelect(final String... columns) {
		final Subquery subquery = new Subquery(columns);
		subqueries.add(subquery);
		return subquery;
	}

	@Override
	public String toString() {
		final SeparatedStringBuilder locallCopy = new SeparatedStringBuilder(this);
		for (final Subquery builder : subqueries) {
			locallCopy.add(" " + builder.queryText() + " ");
		}
		return locallCopy.toString();
	}

	void putAllParametersInThisMap(final Map<String, Object> inHere) {
		for (final Subquery subquery : subqueries) {
			inHere.putAll(subquery.allParameters());
		}
	}
}
