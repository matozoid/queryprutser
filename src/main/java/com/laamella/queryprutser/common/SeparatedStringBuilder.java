package com.laamella.queryprutser.common;

/**
 * Builds a string with the correct prefix, infix and postfix insertions.
 */
public class SeparatedStringBuilder {
    private final StringBuilder builder;
    private final CharSequence infix;
    private final CharSequence postfix;
    private final CharSequence prefix;

    private boolean firstAppend;

    /**
     * "Normal" constructor.
     *
     * @param prefix  a piece of text to put at the start of the eventual string.
     * @param infix   a piece of text to put inbetween each piece of text added.
     * @param postfix a piece of text to put at the end of the eventual string, but
     *                only if the eventual string isn't empty.
     */
    public SeparatedStringBuilder(final CharSequence prefix, final CharSequence infix, final CharSequence postfix) {
        this(prefix, infix, postfix, true, new StringBuilder());
    }

    /**
     * Makes this {@link SeparatedStringBuilder} a copy of source.
     */
    public SeparatedStringBuilder(final SeparatedStringBuilder source) {
        this(source.prefix, source.infix, source.postfix, source.firstAppend, new StringBuilder(source.builder));
    }

    private SeparatedStringBuilder(final CharSequence prefix, final CharSequence infix, final CharSequence postfix, final boolean firstAppend, final StringBuilder builder) {
        Assert.notNull(prefix);
        Assert.notNull(infix);
        Assert.notNull(postfix);
        Assert.notNull(builder);
        this.prefix = prefix;
        this.infix = infix;
        this.postfix = postfix;
        this.firstAppend = firstAppend;
        this.builder = builder;
    }

    /**
     * Adds one piece of text, taking care to insert pre, in, and postfixes.
     *
     * @return this
     */
    public SeparatedStringBuilder add(final CharSequence text) {
        return addWithSpecialInfix(infix, text);
    }

    /**
     * Calls add for each supplied text.
     *
     * @return this
     */
    public SeparatedStringBuilder add(final CharSequence... texts) {
        return addWithSpecialInfix(infix, texts);
    }

    /**
     * Adds one piece of text, taking care to insert pre, in, and postfixes,
     * overriding the infix for just this once.
     *
     * @return this
     */
    public SeparatedStringBuilder addWithSpecialInfix(final CharSequence specialInfix, final CharSequence text) {
        Assert.notNull(specialInfix);
        Assert.notNull(text);
        if (!firstAppend) {
            builder.append(specialInfix);
        } else {
            builder.append(prefix);
        }
        builder.append(text);
        firstAppend = false;
        return this;
    }

    /**
     * Calls addWithSpecialInfix for each supplied text.
     *
     * @return this
     */
    public SeparatedStringBuilder addWithSpecialInfix(final CharSequence specialInfix, final CharSequence... texts) {
        for (final CharSequence text : texts) {
            addWithSpecialInfix(specialInfix, text);
        }
        return this;
    }

    /**
     * @return the string as it was built up to now, as a CharSequence.
     */
    public CharSequence toCharSequence(){
        if (firstAppend) {
            /* Don't even return the postfix */
            return "";
        }
        return builder.append(postfix);
    }

    /**
     * @return the string as it was built up to now.
     */
    @Override
    public String toString() {
        return toCharSequence().toString();
    }
}
