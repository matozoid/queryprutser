package com.laamella.queryprutser.common;

import java.util.HashMap;
import java.util.Map;

/**
 * Select, insert, delete, update, etc.
 */
public abstract class Statement {
    private final Map<String, Object> parameters = new HashMap<String, Object>();

    /**
     * Set a named parameter for this statement.
     */
    public final Statement parameter(final String name, final Object value) {
        Assert.notNull(name);
        parameters.put(name, value);
        return this;
    }

    protected final Map<String, Object> localParameters() {
        return parameters;
    }

    /**
     * @return the current text of the query.
     */
    public abstract CharSequence queryText();

    @Override
    public final String toString() {
        return queryText().toString();
    }

    /**
     * @return all parameters that occur in the query and its subqueries.
     */
    public Map<String, Object> allParameters() {
        final Map<String, Object> allParameters = new HashMap<String, Object>();
        allParameters.putAll(localParameters());
        return allParameters;
    }

    /**
     * Chuck all parameters that this statement holds into the supplied Map.
     */
    public final void putAllParametersInThisMap(Map<String, Object> inHere) {
        inHere.putAll(allParameters());
    }

}
