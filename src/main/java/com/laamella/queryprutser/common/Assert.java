package com.laamella.queryprutser.common;

public final class Assert {
    private Assert() {
        // Singleton.
    }

    public static void notNull(final Object value) {
        if (value == null) {
            throw new AssertionError("Value was null");
        }
    }
}
