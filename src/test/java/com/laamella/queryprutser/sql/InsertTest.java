package com.laamella.queryprutser.sql;

import com.laamella.queryprutser.common.Fuzzy;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class InsertTest {
	@Test
	public void whenNothingIsDoneWithInsertThenItShowsUp() {
		InsertBuilder insert = new InsertBuilder("BLA");
		assertEquals(0, insert.allParameters().size());
		Fuzzy.assertFuzzyString("insert into BLA", insert.queryText());
	}

	@Test
	public void whenOneIntoPropertyIsSetThenItShowsUp() {
		InsertBuilder insert = new InsertBuilder("BLA");
		insert.into.add("x");
		assertEquals(0, insert.allParameters().size());
		Fuzzy.assertFuzzyString("insert into bla (x)", insert.queryText());
	}

	@Test
	public void whenOneSelectPropertyIsSetThenItShowsUp() {
		InsertBuilder insert = new InsertBuilder("BLA");
		insert.select.select("x");
		assertEquals(0, insert.allParameters().size());
		Fuzzy.assertFuzzyString("insert into bla select x", insert.queryText());
	}
}
