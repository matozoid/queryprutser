package com.laamella.queryprutser.sql;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;

import org.junit.Test;

import com.laamella.queryprutser.common.Fuzzy;

public class SelectTest {
	@Test
	public void whenNothingIsDoneWithQueryBuilderThenWeGetNothing() {
		final QueryBuilder queryBuilder = new QueryBuilder("X");
		assertEquals(0, queryBuilder.allParameters().size());
		Fuzzy.assertFuzzyString("SELECT X", queryBuilder.queryText());
	}

	@Test
	public void whenDoingAnAliasedJoinFetchThenWeGetThat() {
		final QueryBuilder select = new QueryBuilder("X");
		select.from.add("X");
		select.from.innerJoin("relationship rel").on("x=y");
		Fuzzy.assertFuzzyString("select x from x inner join relationship rel on x=y", select.queryText());
	}

	@Test
	public void whenUsingUsingThenWeGetThat() {
		final QueryBuilder select = new QueryBuilder("X");
		select.from.add("X");
		select.from.fullOuterJoin("relationship").using("A", "B");
		Fuzzy.assertFuzzyString("select x from x full outer join relationship using (a, b)", select.queryText());
	}

	@Test
	public void whenPuttingASubqueryIntoTheSelectClauseThenItIsThere() {
		final QueryBuilder builder = new QueryBuilder("X");
		builder.select.subSelect("Y");
		Fuzzy.assertFuzzyString("select x, (select y )", builder.queryText());
	}

	@Test
	public void whenPuttingASubqueryIntoTheWhereClauseThenItIsThere() {
		final QueryBuilder builder = new QueryBuilder("X");
		builder.where.subSelect("Y");
		Fuzzy.assertFuzzyString("select x where (select y )", builder.queryText());
	}

	@Test
	public void whenGivingASubqueryABeforeAndAfterStringThenTheyAreThere() {
		final QueryBuilder builder = new QueryBuilder("X");
		final Subquery subselect = builder.where.subSelect("Y");
		subselect.before(">>>");
		subselect.after("<<<");
		Fuzzy.assertFuzzyString("select x where >>>(select y ) <<<", builder.queryText());
	}

	@Test
	public void whenUsingAllClausesThenTheyAreInTheRightOrder() {
		final QueryBuilder builder = new QueryBuilder();
		builder.select.add("[select clause]");
		builder.from.add("[from clause]");
		builder.where.add("[where clause]");
		builder.orderBy("[order by clause]");
		builder.groupBy("[group by clause]");
		builder.having.add("[having clause]");
		Fuzzy.assertFuzzyString("select [select clause] from [from clause] where [where clause] group by [group by clause] having [having clause] order by [order by clause]", builder.queryText());
	}

	@Test
	public void whenUsingMinMaxThenThoseAreAdded() {
		final QueryBuilder builder = new QueryBuilder();
		builder.select.max("a");
		builder.select.min("b");
		Fuzzy.assertFuzzyString("select max(a), min(b)", builder.queryText());
	}

	@Test
	public void whenSelectingMoreThanOneThingThenTheRightInfixesAreUsed() {
		final QueryBuilder builder = new QueryBuilder();
		builder.select("abc", "def", "ghi");
		Fuzzy.assertFuzzyString("select abc, def, ghi", builder.queryText());
	}

	@Test
	public void whenWhereingMoreThanOneThingThenTheRightInfixesAreUsed() {
		final QueryBuilder builder = new QueryBuilder();
		builder.where.and("abc", "def", "ghi");
		Fuzzy.assertFuzzyString("where abc and def and ghi", builder.queryText());
	}

	@Test
	public void whenUsingExistsThenSubqueryIsAdded() {
		final QueryBuilder builder = new QueryBuilder();
		builder.where.exists("A");
		Fuzzy.assertFuzzyString("where exists(select A )", builder.queryText());
	}

	@Test
	public void whenSettingParametersAllOverThePlaceThenTheyAreAllCollected() {
		final QueryBuilder builder = new QueryBuilder("X");
		builder.parameter("a", "a");
		builder.select.subSelect("Y").parameter("b", "b");
		builder.where.subSelect("Z").having.subSelect("O").parameter("c", "c");
		final Map<String, Object> allParameters = builder.allParameters();

		assertEquals(3, allParameters.size());
		assertEquals("a", allParameters.get("a"));
		assertEquals("b", allParameters.get("b"));
		assertEquals("c", allParameters.get("c"));
	}

	@Test
	public void whenCallingToStringThenTheHqlIsReturned() {
		final QueryBuilder builder = new QueryBuilder("X");
		Fuzzy.assertFuzzyString("select x", builder.toString());
	}

	/**
	 * http://stackoverflow.com/questions/1947961/help-me-refactor-this-monster-
	 * of-a-query
	 */
	@Test
	public void whenEnteringAMegaQueryThenItIsPractical() {
		final QueryBuilder builder = new QueryBuilder();
		builder
				.select("OwnerName")
				.sum("AmountPaid", "Paid")
				.sum("AmountOwedComplete", "Owed")
				.sum("AmountOwedThisMonth", "OwedMonth")
				.sum("PaidForPast", "PaidPast")
				.sum("PaidForPresent", "PaidPresent")
				.sum("AmountPaid - PaidForPast - PaidForPresent", "PaidFuture")
				.sum("[Description]");

		final Subquery subquery1 = builder.from.subSelect("OwnerName", "AmountPaid", "AmountOwedComplete", "AmountOwedThisMonth", "PaidForPast", "[Description]");
		subquery1.select.subSelect("CASE WHEN (AmountPaid - PaidForPast) < ABS(AmountOwedThisMonth) THEN AmountPaid - PaidForPast ELSE ABS(AmountOwedThisMonth) END) AS PaidForPresent");

		final Subquery subSelect2 = subquery1.from.subSelect("OwnerName", "AmountPaid", "AmountOwedTotal - AmountPaid AS AmountOwedComplete", " AmountOwedThisMonth");
		subSelect2.select.subSelect("CASE WHEN (AmountPaid < ABS((AmountOwedTotal - AmountPaid)) + AmountOwedThisMonth) THEN AmountPaid ELSE ABS((AmountOwedTotal - AmountPaid)) + AmountOwedThisMonth END) AS PaidForPast");
		subSelect2.select("Description", "TransactionDate");

		System.out.println(builder.queryText());
	}

	// SELECT OwnerName, SUM(AmountPaid) AS Paid, SUM(AmountOwedComplete) AS
	// Owed, SUM(AmountOwedThisMonth) AS OwedMonth,
	// SUM(PaidForPast) AS PaidPast, SUM(PaidForPresent) AS PaidPresent,
	// SUM((AmountPaid - PaidForPast - PaidForPresent)) AS PaidFuture,
	// [Description] FROM (
	// SELECT OwnerName, AmountPaid, AmountOwedComplete, AmountOwedThisMonth,
	// PaidForPast, [Description],
	// (SELECT CASE WHEN (AmountPaid - PaidForPast) < ABS(AmountOwedThisMonth)
	// THEN AmountPaid - PaidForPast
	// ELSE ABS(AmountOwedThisMonth) END) AS PaidForPresent
	// FROM (
	// SELECT OwnerName, AmountPaid, AmountOwedTotal - AmountPaid AS
	// AmountOwedComplete,
	// AmountOwedThisMonth,
	// (SELECT CASE WHEN (AmountPaid < ABS((AmountOwedTotal - AmountPaid)) +
	// AmountOwedThisMonth)
	// THEN AmountPaid ELSE ABS((AmountOwedTotal - AmountPaid)) +
	// AmountOwedThisMonth END) AS PaidForPast,
	// Description, TransactionDate
	/** */
	// FROM (
	// SELECT DISTINCT t.TenantName, p.PropertyName, ISNULL(p.OwnerName,
	// 'Uknown') AS OwnerName, (
	// SELECT SUM(Amount) FROM tblTransaction WHERE
	// Amount > 0 AND TransactionDate >= @StartDate AND TransactionDate <=
	// @EndDate
	// AND TenantID = t.ID AND TransactionCode = trans.TransactionCode
	// ) AS AmountPaid, (
	// SELECT SUM(Amount) FROM tblTransaction WHERE
	// tblTransaction.TransactionCode = trans.TransactionCode AND
	// tblTransaction.TenantID = t.ID
	// ) AS AmountOwedTotal, (
	// SELECT SUM(Amount) FROM tblTransaction WHERE
	// tblTransaction.TransactionCode = trans.TransactionCode AND
	// tblTransaction.TenantID = t.ID
	// AND Amount < 0 AND TransactionDate >= @StartDate AND TransactionDate <=
	// @EndDate
	// ) AS AmountOwedThisMonth, code.Description, trans.TransactionDate FROM
	// tblTransaction trans
	// LEFT JOIN tblTenantTransCode code ON code.ID = trans.TransactionCode
	// LEFT JOIN tblTenant t ON t.ID = trans.TenantID
	// LEFT JOIN tblProperty p ON t.PropertyID = p.ID
	// WHERE trans.TransactionDate >= @StartDate AND trans.TransactionDate <=
	// @EndDate AND trans.Amount > 0
	// ) q
	// ) q2
	// )q3
	// GROUP BY OwnerName, Description

	@Test
	public void whenCreatingAHibernateQueryThenTheRightHqlAndAllParametersAreSet() throws SQLException {
		final QueryBuilder builder = new QueryBuilder("X");
		builder.parameter("a", "a");

		final Connection connectionMock = mock(Connection.class);
		// final PreparedStatement query = builder.query(connectionMock);
		// TODO lots of stuff to assert here for all the named parameter
		// trickery.
		// assertEquals(queryMock, query);
		// verify(queryMock).setProperties(anyMap());
	}
}
