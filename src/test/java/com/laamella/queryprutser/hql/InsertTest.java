package com.laamella.queryprutser.hql;

import com.laamella.queryprutser.common.Fuzzy;
import com.laamella.queryprutser.hql.Insert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class InsertTest {
	@Test
	public void whenNothingIsDoneWithInsertThenItShowsUp() {
		Insert<String, Object> insert = HqlPrutser.insert(String.class, "s", Object.class, "o");
		assertEquals(0, insert.allParameters().size());
		Fuzzy.assertFuzzyString("insert into java.lang.String s from java.lang.Object o", insert.queryText());
	}

	@Test
	public void whenOneIntoPropertyIsSetThenItShowsUp() {
		Insert<String, Object> insert = HqlPrutser.insert(String.class, "s", Object.class, "o");
		insert.insertInto.add("x");
		assertEquals(0, insert.allParameters().size());
		Fuzzy.assertFuzzyString("insert into java.lang.String s (x) from java.lang.Object o", insert.queryText());
	}

	@Test
	public void whenOneSelectPropertyIsSetThenItShowsUp() {
		Insert<String, Object> insert = HqlPrutser.insert(String.class, "s", Object.class, "o");
		insert.select.select("x");
		assertEquals(0, insert.allParameters().size());
		Fuzzy.assertFuzzyString("insert into java.lang.String s select x from java.lang.Object o", insert.queryText());
	}
}
