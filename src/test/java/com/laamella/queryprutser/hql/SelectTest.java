package com.laamella.queryprutser.hql;

import static org.junit.Assert.*;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.*;

import java.util.Map;

import org.hibernate.Query;
import org.hibernate.Session;
import org.junit.Test;

import com.laamella.queryprutser.common.Fuzzy;

public class SelectTest {
	@Test
	public void whenNothingIsDoneWithQueryBuilderThenWeGetASelectEverything() {
		final Select<Object> queryBuilder = HqlPrutser.select(Object.class);
		assertEquals(0, queryBuilder.allParameters().size());
		Fuzzy.assertFuzzyString("from java.lang.Object", queryBuilder.queryText());
	}

	@Test
	public void whenTheRootObjectIsAliasedThenItShowsInTheHql() {
		final Select<Object> queryBuilder = HqlPrutser.select(Object.class, "o");
		Fuzzy.assertFuzzyString("from java.lang.Object o", queryBuilder.queryText());
	}

	@Test
	public void whenDoingAnAliasedJoinFetchThenWeGetThat() {
		final Select<Object> queryBuilder = HqlPrutser.select(Object.class);
		queryBuilder.from.joinFetch("relationship", "rel");
		Fuzzy.assertFuzzyString("from java.lang.Object join fetch relationship rel", queryBuilder.queryText());
	}

	@Test
	public void whenPuttingASubqueryIntoTheSelectClauseThenItIsThere() {
		final Select<Object> builder = HqlPrutser.select(Object.class);
		builder.select.subselect(String.class);
		Fuzzy.assertFuzzyString("select (from java.lang.String ) from java.lang.Object", builder.queryText());
	}

	@Test
	public void whenPuttingASubqueryIntoTheWhereClauseThenItIsThere() {
		final Select<Object> builder = HqlPrutser.select(Object.class);
		builder.where.subselect(String.class);
		Fuzzy.assertFuzzyString("from java.lang.Object where (from java.lang.String )", builder.queryText());
	}

	@Test
	public void whenGivingASubqueryABeforeAndAfterStringThenTheyAreThere() {
		final Select<Object> builder = HqlPrutser.select(Object.class);
		final Subquery<String> subselect = builder.where.subselect(String.class);
		subselect.before(">>>");
		subselect.after("<<<");
		Fuzzy.assertFuzzyString("from java.lang.Object where >>>(from java.lang.String ) <<<", builder.queryText());
	}

	@Test
	public void whenUsingAllClausesThenTheyAreInTheRightOrder() {
		final Select<Object> builder = HqlPrutser.select(Object.class);
		builder.select.add("[select clause]");
		builder.from.add("[from clause]");
		builder.where.add("[where clause]");
		builder.orderBy("[order by clause]");
		builder.groupBy("[group by clause]");
		builder.having.add("[having clause]");
		Fuzzy.assertFuzzyString("select [select clause] from java.lang.Object [from clause] where [where clause] group by [group by clause] having [having clause] order by [order by clause]",
				builder.queryText());
	}

	@Test
	public void whenUsingMinMaxThenThoseAreAdded() {
		final Select<Object> builder = HqlPrutser.select(Object.class);
		builder.select.max("a");
		builder.select.min("b");
		Fuzzy.assertFuzzyString("select max(a), min(b) from java.lang.Object", builder.queryText());
	}

	@Test
	public void whenSelectingMoreThanOneThingThenTheRightInfixesAreUsed() {
		final Select<Object> builder = HqlPrutser.select(Object.class);
		builder.select("abc", "def", "ghi");
		Fuzzy.assertFuzzyString("select abc, def, ghi from java.lang.Object", builder.queryText());
	}

	@Test
	public void whenWhereingMoreThanOneThingThenTheRightInfixesAreUsed() {
		final Select<Object> builder = HqlPrutser.select(Object.class);
		builder.where.and("abc", "def", "ghi");
		Fuzzy.assertFuzzyString("from java.lang.Object where abc and def and ghi", builder.queryText());
	}

	@Test
	public void whenSettingParametersAllOverThePlaceThenTheyAreAllCollected() {
		final Select<Object> builder = HqlPrutser.select(Object.class);
		builder.parameter("a", "a");
		builder.select.subselect(String.class).parameter("b", "b");
		builder.where.subselect(Integer.class).having.subselect(Float.class).parameter("c", "c");
		final Map<String, Object> allParameters = builder.allParameters();

		assertEquals(3, allParameters.size());
		assertEquals("a", allParameters.get("a"));
		assertEquals("b", allParameters.get("b"));
		assertEquals("c", allParameters.get("c"));
	}

	@Test
	public void whenCallingToStringThenTheHqlIsReturned() {
		final Select<Object> builder = HqlPrutser.select(Object.class);
		Fuzzy.assertFuzzyString("from java.lang.Object", builder.toString());
	}

	@Test
	public void whenCreatingAHibernateQueryThenTheRightHqlAndAllParametersAreSet() {
		final Select<Object> builder = HqlPrutser.select(Object.class);
		builder.parameter("a", "a");
		final Session sessionMock = mock(Session.class);
		final Query queryMock = mock(Query.class);
		when(sessionMock.createQuery(((String) argThat(Fuzzy.fuzzy("from java.lang.Object "))))).thenReturn(queryMock);

		final Query query = builder.query(sessionMock);
		assertEquals(queryMock, query);
		verify(queryMock).setProperties(anyMap());
	}
}
