package com.laamella.queryprutser.common;

import static org.junit.Assert.*;

import com.laamella.queryprutser.common.SeparatedStringBuilder;
import org.junit.Test;

public class SeparatedStringBuilderTest {

	@Test
	public void whenNotAppendingAnythingThenResultIsEmpty() {
		final SeparatedStringBuilder string = new SeparatedStringBuilder("pre", "in", "post");
		assertEquals("", string.toString());
	}

	@Test
	public void whenAppendingOneThingThenPreAndPostfixAreAdded() {
		final SeparatedStringBuilder string = new SeparatedStringBuilder("pre", "in", "post");
		string.add("text");
		assertEquals("pretextpost", string.toString());
	}

	@Test
	public void whenAppendingSeveralThingsThenPreInAndPostfixAreAdded() {
		final SeparatedStringBuilder string = new SeparatedStringBuilder("pre", "in", "post");
		string.add("text1", "text2", "text3");
		assertEquals("pretext1intext2intext3post", string.toString());
	}

}
