package com.laamella.queryprutser.common;

import org.mockito.ArgumentMatcher;

import static org.junit.Assert.assertEquals;

public class Fuzzy {
    public static void assertFuzzyString(final CharSequence expected, final CharSequence actual) {
        assertEquals(cleanString(expected), cleanString(actual));
    }

    private static String cleanString(final CharSequence actual) {
        final String[] split = actual.toString().split("\\s+");
        final StringBuffer stringBuffer = new StringBuffer();
        for (final String s : split) {
            stringBuffer.append(s);
            stringBuffer.append(" ");
        }
        return stringBuffer.toString().trim().toLowerCase();
    }

    public static <T> ArgumentMatcher<T> fuzzy(final String expected) {
        return new ArgumentMatcher<T>() {
            @Override
            public boolean matches(Object argument) {
                return cleanString(expected).equals(cleanString(argument.toString()));
            }
        };
    }
}
