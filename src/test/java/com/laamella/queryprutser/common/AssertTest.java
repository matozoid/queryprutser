package com.laamella.queryprutser.common;

import com.laamella.queryprutser.common.Assert;
import org.junit.Test;

public class AssertTest {
	@Test(expected = AssertionError.class)
	public void whenAssertNotNullOnNullThenGetAnAssertionError() {
		Assert.notNull(null);
	}

	@Test
	public void whenAssertNotNullOnNotNullThenNothingHappens() {
        Assert.notNull("");
	}
}
